<?php

/**
 * @file
 * This module exposes user operations for modifying Role Expire data.
 * @author Michael Prasuhn <mike@shomeya.com>
 * @link http://www.shomeya.com
 * @package role_expire_operations
 */

/**
 * Form builder for admin settings form.
 *
 * @param <type> $form_state
 */
function role_expire_operations_settings($form_state) {
  $form = array();

  $form['instructions'] = array(
    '#type' => 'markup',
    '#weight' => -100,
    '#value' => t('Enter a name to display in the bulk operations menu, a role to operate on, and a time following the example listed below. To delete an item, remove the name, and save the form.')
  );

  if ((isset($form_state['values']['ops'])) && !empty($form_state['values']['ops'])) {
    $operations = $form_state['values']['ops'];
  }
  else {
    $operations = variable_get('role_expire_operations_settings', array());
  }


  if (isset($form_state['ops_count'])) {
    $count = $form_state['ops_count'];
  }
  else {
    $count = max(1, empty($operations) ? 1 : count($operations));
  }

  $form['op_wrapper'] = array(
    '#weight' => -1,
    '#prefix' => '<div class="clear-block" id="role-op-wrapper">',
    '#suffix' => '</div>'
  );


  $form['op_wrapper']['ops'] = array(
    '#prefix' => '<div id="role-ops">',
    '#suffix' => '</div>',
    '#theme' => 'role_expire_operations_ops_item'
  );

  for ($delta = 0; $delta < $count; $delta++) {
    $name = isset($operations[$delta]['name']) ? $operations[$delta]['name'] : '';
    $role = isset($operations[$delta]['role']) ? $operations[$delta]['role'] : 1;
    $time = isset($operations[$delta]['time']) ? $operations[$delta]['time'] : '';

    $form['op_wrapper']['ops'][$delta] = _role_expire_operations_form_item($delta, $name, $role, $time);
  }

  $form['op_wrapper']['ops_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add another operation'),
    '#description' => t("If the amount of boxes above isn't enough, click here to add another operation."),
    '#weight' => 1,
    '#submit' => array('role_expire_operations_more_submit'), // If no javascript action.
    '#ahah' => array(
      'path' => 'role_expire_operations/js',
      'wrapper' => 'role-ops',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#validate' => array('role_expire_operations_settings_form_validate'),
    '#submit' => array('role_expire_operations_settings_form_submit'),
  );

  return $form;
}

/**
 * Helper function to return an individual operation form.
 *
 * @param <type> $delta
 * @param <type> $name
 * @param <type> $role
 * @param <type> $time
 * @return <type>
 */
function _role_expire_operations_form_item($delta, $name, $role, $time) {
  $roles = user_roles();
  unset($roles[1], $roles[2]);

  $form = array(
    '#tree' => TRUE
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $name,
    '#size' => 40,
    '#description' => t('The name to display in the operations list.'),
    '#parents' => array('ops', $delta, 'name'),
  );

  $form['role'] = array(
    '#type' => 'select',
    '#title' => t('Role'),
    '#options' => $roles,
    '#default_value' => $role,
    '#description' => t('The role affected by this operation.'),
    '#parents' => array('ops', $delta, 'role'),
  );

  $form['time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time'),
    '#default_value' => $time,
    '#size' => 20,
    '#description' => t('Enter date and time to associate with this operation in format: <br /><em>dd-mm-yyyy hh:mm:ss</em> or use relative time <br />i.e. 1 day, 2 months, 1 year, 3 years.'),
    '#parents' => array('ops', $delta, 'time'),
  );

  return $form;
}

/**
 * Submit handler for add more operations button.
 *
 * @param <type> $form
 * @param <type> $form_state
 */
function role_expire_operations_more_submit($form, &$form_state) {
  $foo = TRUE;

  if ($form_state['values']['ops_more']) {
    $form_state['ops_count'] = count($form_state['values']['ops']) + 1;
  }
}

/**
 * JS callback for rebuilding form.
 */
function role_expire_operations_form_js() {
  // Get form build id and setup state.
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];

  // Get form from cache.
  $form = form_get_cache($form_build_id, $form_state);

  // Get ready to process form.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // Process form and save updated info.
  drupal_process_form($form_id, $form, $form_state);

  // Rebuild form, cache it, but do not process it again.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  // Pick the bits we need from the form, and render them.
  $ops_form = $form['op_wrapper']['ops'];
  unset($ops_form['#prefix'], $ops_form['#suffix']);
  $output = theme('status_messages') . drupal_render($ops_form);

  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Theme function for role ops table.
 *
 * @param <type> $form
 * @return <type>
 */
function theme_role_expire_operations_ops_item($form) {
  $rows = array();
  $headers = array(
    t('Name'),
    t('Role'),
    t('Time'),
  );

  foreach (element_children($form) as $key) {
    // No need to print the field title every time.
    unset($form[$key]['name']['#title'], $form[$key]['role']['#title'], $form[$key]['time']['#title']);

    $row = array(
      'data' => array(
        array('data' => drupal_render($form[$key]['name']), 'class' => 'op-name'),
        array('data' => drupal_render($form[$key]['role']), 'class' => 'op-role'),
        array('data' => drupal_render($form[$key]['time']), 'class' => 'op-time'),
      ),
    );

    if (isset($form[$key]['#attributes'])) {
      $row = array_merge($row, $form[$key]['#attributes']);
    }
    $rows[] = $row;
  }

  $output = theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Submit handler for settings form.
 */
function role_expire_operations_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['ops']) && is_array($form_state['values']['ops'])) {
    $items = $form_state['values']['ops'];

    foreach ($items as $key => $value) {
      if (empty($value['name'])  || empty($value['time'])) {
        unset($items[$key]);
      }
    }
    $items = array_merge($items);
    variable_set('role_expire_operations_settings', $items);

    drupal_set_message(t('The configuration options have been saved.'));
  }
}

 /**
  * Validation handler for settings form.
  */
function role_expire_operations_settings_form_validate($form, &$form_state) {
//   dsm($form);
  foreach ($form_state['values']['ops'] as $key => $value) {
    if (!strtotime($value['time'])) {
      form_set_error('op_wrapper][ops]['. $key, t('The time %time is not valid', array('%time' => $value['time'])));
    }
  }
}